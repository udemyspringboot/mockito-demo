package com.in28minutes.mockito.mockitodemo;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;

public class SomeBusinessMockTest {

	@Test
	public void testFindTheGreatestFromAllData() {
		final DataService dataServiceMock = mock(DataService.class);
		when(dataServiceMock.retrieveAllData()).thenReturn(new int[] { 24, 5, 16 });
		final SomeBusinessImpl someBusinessImpl = new SomeBusinessImpl(dataServiceMock);
		final int result = someBusinessImpl.findTheGreatestFromAllData();
		assertEquals(24, result);
	}

	@Test
	public void testFindTheGreatestFromAllData_ForOneValue() {
		final DataService dataServiceMock = mock(DataService.class);
		when(dataServiceMock.retrieveAllData()).thenReturn(new int[] { 15 });
		final SomeBusinessImpl someBusinessImpl = new SomeBusinessImpl(dataServiceMock);
		final int result = someBusinessImpl.findTheGreatestFromAllData();
		assertEquals(15, result);
	}
}