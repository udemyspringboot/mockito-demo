package com.in28minutes.mockito.mockitodemo;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SomeBusinessMockAnnotationTest {

	@Mock
	private DataService dataServiceMock;

	@InjectMocks
	private SomeBusinessImpl someBusinessImpl;

	@Test
	public void testFindTheGreatestFromAllData() {
		when(this.dataServiceMock.retrieveAllData()).thenReturn(new int[] { 24, 5, 16 });
		final SomeBusinessImpl someBusinessImpl = new SomeBusinessImpl(this.dataServiceMock);
		assertEquals(24, someBusinessImpl.findTheGreatestFromAllData());
	}

	@Test
	public void testFindTheGreatestFromAllData_ForOneValue() {
		when(this.dataServiceMock.retrieveAllData()).thenReturn(new int[] { 15 });
		final SomeBusinessImpl someBusinessImpl = new SomeBusinessImpl(this.dataServiceMock);
		assertEquals(15, someBusinessImpl.findTheGreatestFromAllData());
	}

	@Test
	public void testFindTheGreatestFromAllData_NoValues() {
		when(this.dataServiceMock.retrieveAllData()).thenReturn(new int[] {});
		final SomeBusinessImpl someBusinessImpl = new SomeBusinessImpl(this.dataServiceMock);
		assertEquals(Integer.MIN_VALUE, someBusinessImpl.findTheGreatestFromAllData());
	}
}