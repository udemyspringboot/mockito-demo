package com.in28minutes.mockito.mockitodemo;

public class SomeBusinessImpl {

	private DataService dataService;
	
	public SomeBusinessImpl(final DataService dataService) {
		super();
		this.dataService = dataService;
	}

	int findTheGreatestFromAllData() {
		final int[] retrieveAllData = this.dataService.retrieveAllData();
		int greatest = Integer.MIN_VALUE;
		for (final int value : retrieveAllData) {
			if (value > greatest) {
				greatest = value;
			}
		}
		return greatest;
	}
}
